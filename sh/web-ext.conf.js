module.exports = {
    // Global options:
    verbose: true,
    ignoreFiles: [
        'py/**',
        'sh/**',
        'bin/**',
        'assets/captures/**'
    ],
    // Command options:
    build: {
        overwriteDest: true,
    }
};