from facebookads.api import FacebookAdsApi
from facebookads.adobjects.targetingsearch import TargetingSearch

import private
import json


api = FacebookAdsApi.init(private.APP_ID,
                          private.APP_SECRET,
                          private.ACCESS_TOKEN)

params = {
    'q': '',
    'limit': 1000,
    'type': 'adgeolocation',
    'location_types': ['country'],
}

countries = TargetingSearch.search(params=params)
out = []
for country in countries:
    out.append({
        'name': country['name'],
        'code': country['country_code']})

with open('targets/countries.json', 'w+') as jsonfile:
    json.dump(out, jsonfile, indent=2)

params = {
    'q': '',
    'limit': 1000,
    'type': 'adlocale'
}

locales = TargetingSearch.search(params=params)
out = []
for locale in locales:
    out.append({
        'name': locale['name'],
        'key': locale['key']})

with open('targets/locales.json', 'w+') as jsonfile:
    json.dump(out, jsonfile, indent=2)
