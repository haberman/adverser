from facebookads.api import FacebookAdsApi
from facebookads.adobjects.targetingsearch import TargetingSearch

from zipfile import ZipFile

import os
import glob
import json
import shutil
import private

api = FacebookAdsApi.init(private.APP_ID,
                          private.APP_SECRET,
                          private.ACCESS_TOKEN,
                          private.ACCOUNT_ID)

locales = ['es_ES', 'en_US', 'fr_FR', 'tr_TR', 'sv_SE',
           'af_ZA', 'sq_AL', 'ar_AR', 'hy_AM', 'az_AZ', 'eu_ES', 'be_BY', 'bn_IN', 'bs_BA', 'bg_BG', 'ca_ES',
           'hr_HR', 'cs_CZ', 'da_DK', 'nl_NL', 'nl_BE', 'en_PI', 'en_GB', 'en_UD', 'eo_EO', 'et_EE', 'fo_FO', 'tl_PH', 'fi_FI',
           'fr_CA', 'gl_ES', 'ka_GE', 'de_DE', 'el_GR', 'gn_PY', 'gu_IN', 'he_IL', 'hi_IN', 'hu_HU', 'is_IS', 'id_ID',
           'ga_IE', 'it_IT', 'ja_JP', 'jv_ID', 'kn_IN', 'kk_KZ', 'km_KH', 'ko_KR', 'ku_TR', 'la_VA', 'lv_LV', 'fb_LT',
           'lt_LT', 'mk_MK', 'mg_MG', 'ms_MY', 'ml_IN', 'mt_MT', 'mr_IN', 'mn_MN', 'ne_NP', 'nb_NO', 'nn_NO', 'ps_AF', 'fa_IR',
           'pl_PL', 'pt_BR', 'pt_PT', 'pa_IN', 'ro_RO', 'ru_RU', 'sr_RS', 'zh_CN', 'sk_SK', 'sl_SI', 'so_SO',
           'es_LA', 'sw_KE', 'sy_SY', 'tg_TJ', 'ta_IN', 'te_IN', 'th_TH',
           'zh_HK', 'zh_TW', 'uk_UA', 'ur_PK', 'uz_UZ', 'vi_VN', 'cy_GB']

ad_classes = ['interests',
              'behaviors',
              'demographics',
              'life_events',
              'politics',
              'industries',
              'income',
              'net_worth',
              'home_type',
              'home_ownership',
              'generation',
              'household_composition',
              'moms',
              'office_type',
              'family_statuses',
              'user_device']

for locale in locales:
    target_folder = 'targets/res/{}'.format(locale)
    zip_name = 'targets/res/{}.zip'.format(locale)

    if os.path.isfile(zip_name):
        print('passed -> {}'.format(zip_name))
        continue

    if os.path.isdir(target_folder):
        shutil.rmtree(target_folder)

    os.mkdir(target_folder)

    for ad_class in ad_classes:
        params = {
            'type': 'adTargetingCategory',
            'class': ad_class,
            'locale': locale
        }

        targets = TargetingSearch.search(params=params)
        if len(targets) is 0:
            continue

        out = []
        for target in targets:
            res = {}

            for key in target.keys():
                res[key] = target[key]

            out.append(res)

        with open('{}/{}.json'.format(target_folder, ad_class), 'w+') as json_file:
            json.dump(out, json_file, indent=2)

    with ZipFile('targets/res/{}.zip'.format(locale), 'w') as archive:
        for target in glob.glob('{}/*.json'.format(target_folder)):
            arcname = os.path.basename(target)
            archive.write(target, arcname=arcname)

    shutil.rmtree(target_folder)

    print('packed -> {}'.format(locale))
