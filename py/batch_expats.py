from facebookads.api import FacebookAdsApi
from facebookads.adobjects.adaccount import AdAccount
from facebookads.adobjects.adset import AdSet

import json
import glob
import os.path
import private

expats = json.load(open('expats/raw/expats_raw-all.json'))
api = FacebookAdsApi.init(private.APP_ID,
                          private.APP_SECRET,
                          private.ACCESS_TOKEN)
batch_api = api.new_batch()
account = AdAccount(private.ACCOUNT_ID)


def batch(batch_api, country, start, stop):
    res = []

    for i in range(start, stop):
        print(expats[i]['name'])
        expat_id = expats[i].get('id')

        params = {
            'targeting_spec': {
                'geo_locations': {'countries': [country]},
                'user_adclusters': [{'id': expat_id}]
            }
        }

        res.append({
            'original': expats[i],
            'req': account.get_reach_estimate(params=params, batch=batch_api)
        })

    batch_api.execute()
    return res


# ue27 country codes:
# AT, BE, CY, CZ, DE, DK, EE, ES, FI, FR, GB, GR, HU, IE, IT, LU, LV, MC, MT, NL, PL, PT, RO, SE, SI, SK

country = 'AT'
localized_file = 'expats/localized/expats_{}.json'.format(country)
start = 0
stop = 48
out = []

if os.path.isfile(localized_file):
    start = 48
    stop = len(expats)
    out = json.load(open(localized_file))

for b in batch(api.new_batch(), country, start, stop):
    reach_estimate = b['req'].load()

    if reach_estimate is not None:
        b['original']['audience_size'] = reach_estimate[0]['users']
        out.append(b['original'])

with open(localized_file, 'w+') as outfile:
    json.dump(out, outfile, indent=2)
