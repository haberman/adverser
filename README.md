# Adverser 0.8.2
The Adverser can retrieve the list of your personal interests and pack it with those that Facebook uses for targeting audience of their advertisements.

### Using
- **Firefox plugin**:
For developers: type exactly `about:debugging` in Firefox and locate `manifest.json` by clicking on _Load Temporary Add-on_. Enable add-on debugging and click _Debug_ to see logs. Also, use the `build.sh` one-liner script to zip the plugin. For users: install with one of signed `xpi` files located in the [bin](https://framagit.org/haberman/adverser/tree/master/bin) directory or install from [public host](https://addons.mozilla.org/en-US/firefox/addon/adverser)!
- **Marketing API**:
The `py` folder contains python code responsible for fetching data from the [marketing api](https://developers.facebook.com/docs/marketing-apis). A `private` module needs to be created so the `__init__.py` file contains required developpers' credentials:

```python
APP_ID = '***'
APP_SECRET = '***'
ACCESS_TOKEN = '***'
ACCOUNT_ID = '***'
```

`batch_expats.py` will fetch statistics for every expatriated people within the ue-27. It exposes, more generally, how one should use the marketing API to get audience for a specific set of `fbids`. 

`batch_targets.py` will fetch data about every facebook's advertising classes with translated description for more than 90 languages. This script is responsible for feeding zips to our [targets API](http://ext.penumbra.me/adverser/api/?locale=en_US).

`list_language.py` simply grabs facebook's i18n coverage and localized metadata.

### Todo's
1. [x] merges `deshred` API to working code.
2. [x] internationalizes UI.
3. [x] fix listener not being removed when leaving one of the listened URLs.
4. [x] make the reload button open the `Ad preferences` page when active tab is not there or simply reload it when already on it.
5. [x] fix popup glitch that reads i18n messages wrong.
6. [x] prevent page from being reloaded twice when action is requested from popup.
7. [x] append `lang` attribute to popup html with localized value.
8. [x] implement dowload capability as an archive pack of matrix, traits and id, all both in `csv` and `json` format.
9. [x] We're currently using the browser locale to guess the matrix language. This is wrong and need to be fixed by grabbing the facebook language setting (browser locale should be used as fallback though...)
10. [x] grab more data when landing on facebook home page, store that as an addtitional (custom) `user.id.json`.
11. [x] gently tells client that we have nothing to download when thr's no'in 2 downlo'.
12. [x] make the download text reflects what's actually available for download.
13. [x] #v1 implement the ability to feed a shared database of i18n'ed categories' matrix;
14. [x] #v1 so: code a unified API to store / serve matrices files;
15. [x] on the `marketing api`: auto-zip files and delete temporary `json`'s.
16. [x] update API with zip of every known localized targets;
17. [x] #v1 in the popup, as a proof of concept, inject some quick generated poetry out of data cross-referencing;
- [ ] #v1 when landing on Facebook news stream, listen to DOM change and find sponsored content container so we could clear it with some art;

### History
- [11/04/2018] -> solved `1`, `2` by following `i18n` [guidelines](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Internationalization), `3`, `4`, `5`, `6`, `7`, `8`
- [12/04/2018] -> checked `9`, `10`, `11`, `12`
- [17/04/2018] -> checked `13`
- [30/04/2018] -> checked `14`: targets are now fetched using the Marketing API, which gives official audience size; only personnal data retrieval remains, necessarily, kinda hacky...
- [14/05/2018] -> checked `15`, `16`
- [15/05/2018] -> checked `17`

### Coding style
- I like Python so I don't use semicolons, which makes my code unminifiable (I know).
- There's no such thing as access modifiers in javascript, but I tend to reproduce a private/public interface for readability by appending '_' on things that aren't meant to be accessed outside their scopes.