const URL_TRAITS = 'https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen'
const URL_FACEBOOK = 'https://www.facebook.com/'

/** Holds a port which enables communications with the popup. */
let port_ = null

/** Shortcut to get the active tab. */
const ux_active_tab =
    async () => {
        const active_tab = await browser.tabs.query({ active: true, currentWindow: true })
        return active_tab[0]
    }

/**
* Updates the plugin interface, meaning: icon state, badge count and popup DOM (in case we have an open port to it).
*
* @param {object} user Stored user
* @param {array} traits Stored interests' traits
*/
const ux_update =
    async (user, traits) => {
        let amount = 0
        if (traits) { amount = traits.attached.length + traits.clicked.length + traits.interests.length }

        if (amount != 0) {
            browser.browserAction.setBadgeBackgroundColor({ color: 'rgb(75,75,75)' })
            browser.browserAction.setBadgeText({ text: amount.toString() })
        }

        if (port_ && !browser.runtime.lastError) {
            if (targets_.zip() == null) { await targets_.download(user.locale) }

            let download_content = ''
            if (amount != 0) { download_content = browser.i18n.getMessage('ui_items', [amount]) }
            else {
                const downloadables = []
                if (targets_.zip() != null) { downloadables.push(browser.i18n.getMessage('ui_targets', [user.locale])) }
                if (user.id != null) { downloadables.push('id') }

                download_content = downloadables.reduce((acc, curr) => `${acc} + ${curr}`)
            }

            const reply = {
                user: user,
                count: traits ? traits.length : 0,
                download_content: download_content,
                reload_title: browser.i18n.getMessage('ui_reload_traits'),
                download_title: browser.i18n.getMessage('ui_download_archive')
            }

            if (targets_.zip() != null && traits != null) {
                reply.data_story = await targets_.datastory(traits)
            }

            port_.postMessage({ from: 'background', action: 'update', reply: reply })
            return true
        }

        return false
    }

/**
 * Callback to navigation change.
 * -> starts / stops network sniffing depending on the URL.
 * 
 * @param {int} listening If not `-1`, will start listening on TRAITS and redirects active tab there
 * @returns {int} Id of the active tab on which navigation occured.
 */
const ux_navigation =
    async listening => {
        const active_tab = await ux_active_tab()
        const stored_user = await storage_.user()
        let network_listening = network_.listening()

        if (listening == -1) {
            const url = active_tab.url

            if (url.includes(URL_TRAITS)) { network_listening = LISTENING_TRAITS }
            else if (url.includes(URL_MATRIX)) { network_listening = LISTENING_MATRIX }
            else if (url.includes(URL_FACEBOOK)) { network_listening = LISTENING_FACEBOOK }
            else { network_listening = LISTENING_IDLE }
        }

        if (network_listening != LISTENING_IDLE || listening != -1) {
            await action_.state(STATE_LISTEN)
            await storage_.save_state(STATE_LISTEN)
            if (network_listening == LISTENING_FACEBOOK && stored_user.user != null) { return false } // don't reload facebook if we already IDed user.
            if (!network_.has_listener()) { network_.listen(listening == -1 ? network_listening : listening) }
            if (listening == -1) { await browser.tabs.reload(active_tab.id) }
            else { await browser.tabs.update(active_tab.id, { url: URL_TRAITS }) }
        } else {
            await action_.state(STATE_DEFAULT)
            await storage_.state(STATE_DEFAULT)

            network_.listen(LISTENING_IDLE)
        }

        return active_tab.id
    }

/** 
 * Packs what's available into a zip that's immediately queued to download.
 * 
 * @param {object} user Stored user
 * @param {object} traits Stored traits
 * @returns {boolean} This will return `false` is case of download failure or data unavailability.
*/
const ux_download =
    async (user, traits) => {

        const root_folder = `adverser_${Date.now()}`
        const zip = new JSZip()
        zip.folder(root_folder)

        const targets_zip = targets_.zip()
        if (targets_zip != null) {
            for (let file of Object.keys(targets_zip.files)) {
                const json_str = await targets_zip.file(file).async('string')
                const json = JSON.parse(json_str)
                zip.file(`${root_folder}/targets/${file}`, data_to_json(json))
                zip.file(`${root_folder}/targets/${file.replace('.json', '.csv')}`, data_to_csv(json))
            }
        }

        if (traits) {
            if (traits.interests) {
                zip.file(`${root_folder}/traits/interests.json`, data_to_json(traits.interests))
                zip.file(`${root_folder}/traits/interests.csv`, data_to_csv(traits.interests))
            }

            if (traits.attached) {
                zip.file(`${root_folder}/traits/attached.json`, data_to_json(traits.attached))
                zip.file(`${root_folder}/traits/attached.csv`, data_to_csv(traits.attached))
            }

            if (traits.clicked) {
                zip.file(`${root_folder}/traits/clicked.json`, data_to_json(traits.clicked))
                zip.file(`${root_folder}/traits/clicked.csv`, data_to_csv(traits.clicked))
            }
        }

        if (user.id) {
            zip.file(`${root_folder}/${user.id.id}.id.json`, data_to_json(user.id))
            zip.file(`${root_folder}/${user.id.id}.id.csv`, data_to_csv([user.id]))
        }

        if (user || targets_ || traits) {
            const blob = await zip.generateAsync({ type: 'blob' })
            const url = URL.createObjectURL(blob)

            try { await browser.downloads.download({ url: url, filename: `${root_folder}.zip` }) }
            catch (e) { return false }
        } else { return false }

        return true
    }

/**
 * Method passed to the network as a callback for valid data reception.
 * 
 * @param {object} info An object holding an `edge` key telling us what to do with `data`
 */
const data_callback =
    async info => {
        if (!info.hasOwnProperty('edge')) { return false }

        const stored_user = await storage_.user()
        switch (info.edge) {
            case 'traits':
                const stored_traits = await storage_.sync_traits(info.data)
                const handle = await ux_update(stored_user, stored_traits)
                return handle
                break

            case 'user':
                let locale = info.data.locale
                if (locale.indexOf('-') != -1) { locale = locale.replace('-', '_') }

                await storage_.sync_user(info.data.user, locale)
                break

            case 'user':
                await storage_.sync_matrix(info.data)
                break
        }

        return true
    }

/**
* Encodes an array of objects into a blob of its corresponding csv representation.
* For every objects, the biggest one will be used as a reference to inject an header at the beginning.
* 
* @param {array} items The array to encode
* @returns {string} A csv encoded string.
*/
const data_to_csv =
    items => {
        // 1st pass -> gets biggest header
        let header = null
        items.map(
            item => {
                const keys = Object.keys(item)
                if (header == null || header.length < keys.length) { header = keys }
            })

        // 2nd pass -> writes data
        let csv = ''
        items.map(
            item => {
                csv += Object.keys(item).reduce((previous, key) => previous + item[key] + ',', '') + '\r\n'
            })

        return `${header.join(',')}\r\n${csv}`
    }

/** Little JSON beautifier.  */
const data_to_json = items => JSON.stringify(items, null, 2)


const storage_ = storage(browser.storage, browser.i18n.getUILanguage())
const action_ = action(browser.browserAction)
const network_ = network(browser.webRequest, null, data_callback)
const targets_ = targets()

/**
 * When plugin gets installed
 * -> inits storage and restores ui.
 */
browser.runtime.onInstalled.addListener(
    async e => {
        await storage_.init()
        await action_.init()
        return true
    })

/**
 * When plugin action button gets clicked
 * -> stores new state and reload ui.
 */
browser.browserAction.onClicked.addListener(
    async e => {
        const state = await action_.click()
        await storage_.state(state)
        return true
    })

/**
 * Listens to messages posted on background port and triggers actions upon'em:
 * -> when issued from popup: returns stats to reload its DOM or pack storage as a file to be downloaded.
 */
browser.runtime.onConnect.addListener(
    async port => {
        port_ = port

        port.onMessage.addListener(
            async request => {
                if (request.target == 'popup') {
                    const stored_user = await storage_.user()
                    const stored_traits = await storage_.traits()

                    if (targets_.zip() == null) { await targets_.download(stored_user.locale) }

                    switch (request.action) {
                        case 'open':
                            const handle = await ux_update(stored_user, stored_traits)
                            return handle
                            break
                        case 'reload':
                            await ux_navigation(LISTENING_TRAITS)
                            break
                        case 'download':
                            const download = await ux_download(stored_user, stored_traits)
                            if (!download) {
                                const reply = {
                                    level: 'warn',
                                    msg: browser.i18n.getMessage('warn_nodata')
                                }

                                port_.postMessage({ from: 'background', action: 'log', reply: reply })
                            }

                            break
                    }

                    return true
                }

                return false
            })


        port.onDisconnect.addListener(
            port => {
                port_ = null;
                if (port.error) { console.warn(`disconnected because of error: ${port.error}`) }
            })
    })