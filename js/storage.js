/**
 * Handles interactions with the persistent storage API so we can
 * keep a track of user settings and resources.
 * 
 * @param {browser.storage} object For now storage use the `local` flavor of browser storage
 * @param {string} locale Language tag to identify the categories' matrix
 * @see https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/storage 
 */
const storage = (browser_storage, locale) => {
    return {
        init: () => storage_init_(browser_storage, locale),
        user: () => storage_read_(browser_storage, KEY_USER),
        traits: () => storage_read_(browser_storage, KEY_TRAITS),
        save_state: value => storage_save_state_(browser_storage, value),
        sync_traits: value => storage_sync_traits_(browser_storage, value),
        sync_user: (user, locale) => storage_sync_user_(browser_storage, user, locale)
    }
}

const KEY_USER = 'adverser_user'
const KEY_TRAITS = 'adverser_traits'

/**
 * Inits by storing some defaults.
 * 
 * @param {string} locale The browser initial locale
 */
const storage_init_ =
    async (storage, locale) => {
        const default_user = { state: STATE_DEFAULT, locale: locale, id: null }

        await storage.local.set({ adverser_user: default_user })

        const stored_user = await storage_read_(storage, KEY_USER)
        return stored_user
    }

/**
 * Returns what's stored on a given key.
 * 
 * @param {string} key The key...
 * @returns {object} What is stored... 
 */
const storage_read_ =
    async (storage, key) => {
        const stored = await storage.local.get(key)

        if (stored.hasOwnProperty(key)) { return stored[key] }
        else { return null }
    }

/**
 * Stores a new state.
 * 
 * @param {int} value The new state as an integer
 * @returns The stored settings.
 */
const storage_save_state_ =
    async (storage, value) => {
        let stored_user = await storage_read_(storage, KEY_USER)
        stored_user.state = value

        await storage.local.set({ adverser_user: stored_user })

        stored_user = await storage_read_(storage, KEY_USER)
        return stored_user
    }

/**
* Merges user and locale to stored settings.
* 
* @param {object} user A facebook user object as stripped by network
* @param {string} local The language tag as set on client facebook preference
* @returns {object} The new settings.
*/
const storage_sync_user_ =
    async (storage, user, locale) => {
        let stored_user = await storage_read_(storage, KEY_USER)
        stored_user.locale = locale
        stored_user.id = { id: user.id, name: user.name, gender: user.gender, picture: user.profile_picture.uri }

        await storage.local.set({ adverser_user: stored_user })

        stored_user = await storage_read_(storage, KEY_USER)
        return stored_user
    }

/**
 * Syncs an incoming array of fresh vaa items and stores them if not already present.
 * 
 * @param {array} value The actual content of stored vaa items
 * @returns The stored settings.
 */
const storage_sync_traits_ =
    async (storage, value) => {
        let stored_traits = await storage_read_(storage, KEY_TRAITS)

        // immediately stores the first time we hit storage
        if (stored_traits == null) {
            await storage.local.set({ adverser_traits: value })
            stored_traits = await storage_read_(storage, KEY_TRAITS)
        }

        if (stored_traits == null) { return null }

        // filters incoming items and keep uniques only.
        const new_attached_ads = storage_merge_(stored_traits.attached, value.attached)
        const new_clicked_ads = storage_merge_(stored_traits.clicked, value.clicked)
        const new_interests = storage_merge_(stored_traits.interests, value.interests)
        const new_traits = { attached: new_attached_ads, clicked: new_clicked_ads, interests: new_interests }

        await storage.local.set({ adverser_traits: new_traits })
        stored_traits = await storage_read_(storage, KEY_TRAITS)

        return stored_traits
    }

const storage_merge_ =
    (stored, incoming) => {
        const merge = []

        for (let value of incoming) {
            if (stored.findIndex(el => el.fbid == value.fbid) == -1) { merge.push(value) }
        }

        return stored.concat(merge)
    }