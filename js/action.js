/**
 * Handles interactions with the browserAction API, meaning: UX design of the plugin's button.
 * 
 * @param {browser.browserAction} object
 * @see https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/browserAction 
 */
const action = (browser_action) => {
    return {
        init: () => action_init_(browser_action),
        click: () => action_state_(action, state_),
        state: state => action_state_(browser_action, state)
    }
}

/** plugin is watching navigation */
const STATE_DEFAULT = 0;

/** pulgin is watching network */
const STATE_LISTEN = 1;

/** internal state */
let state_ = STATE_DEFAULT

/** Inits by adding listeners and reloading ui to refect saved settings. */
const action_init_ =
    action => {
        action_state_(action, STATE_DEFAULT)
        action.setTitle({ title: 'adverser' })
        return state_
    }

/** Reloads browser action ui with a new state. */
const action_state_ =
    (action, state) => {
        if (state == state_) { return } // avoid icon flickering

        state_ = state
        let icon = 'assets/'

        switch (state_) {
            case STATE_DEFAULT:
                icon += 'adverser.svg'
                break
            case STATE_LISTEN:
                icon += 'adverser_listen.svg'
                break
        }

        action.setIcon({ path: icon })
        return state
    }
