/** Connects to background and posts an open status update. */
const port = browser.runtime.connect(browser.runtime.getManifest().applications.gecko.id)
port.postMessage({ target: 'popup', action: 'open' })

const INFO_VISIBILITY_TIMEOUT = 5000 // 2 lines of info

/** Shows popup info div with a new message, auto-hide after 5s. */
const log =
    (msg, level) => {
        const popup_info = document.querySelector('div.popup-info')
        popup_info.classList.add(`info-${level}`)
        popup_info.classList.remove(`hidden`)
        popup_info.textContent = msg

        window.setTimeout(() => popup_info.classList.add('hidden'), INFO_VISIBILITY_TIMEOUT)
    }

let datastory_content = null
let datastory_char = 0

const story_read = () => {
    const p_datastory = document.querySelector('#datastory')
    const content = p_datastory.textContent
    if (content.length > 0) {
        p_datastory.textContent = content.slice(1)
    } else {
        p_datastory.setAttribute('style', '')
        p_datastory.textContent = datastory_content
    }
}

let timer_id = null

/** Updates stats with new info from background script. */
const update =
    info => {
        const user = info.reply.user

        let icon = '../assets/'
        switch (user.state) {
            case 0:
                icon += 'adverser.svg'
                break
            case 1:
                icon += 'adverser_listen.svg'
                break
        }

        const bt_download = document.querySelector('a.button-download')
        const p_datastory = document.querySelector('#datastory')

        /** Only updates UI if download content is different */
        if (info.reply.download_content != bt_download.textContent) {
            bt_download.textContent = info.reply.download_content
            bt_download.setAttribute('data-count', info.reply.count)
            bt_download.setAttribute('title', info.reply.download_title)

            document.querySelector('html').setAttribute('lang', user.locale.substring(0, 2))
            document.querySelector('a.button-reload').setAttribute('title', info.reply.reload_title)
            document.querySelector('#state_icon').setAttribute('data', icon)
        }

        if (!info.reply.hasOwnProperty('data_story')) { return }
        if (datastory_content == null || datastory_content != info.reply.data_story) {
            datastory_content = info.reply.data_story
            datastory_char = 0

            if (timer_id == null) { timer_id = setInterval(story_read, 150) }
        }
    }

/** 
 * Answers to update requests.
 * -> updates DOM
 */
port.onMessage.addListener(
    info => {
        if (info.action == 'update') { update(info) }
        else if (info.action == 'log') { log(info.reply.msg, info.reply.level) }

    })

/** When DOM is loaded, starts listening to relevant events. */
window.addEventListener('DOMContentLoaded',
    () => {

        document.querySelector('a.button-download')
            .addEventListener('click', e => port.postMessage({ target: 'popup', action: 'download' }))

        document.querySelector('a.button-reload')
            .addEventListener('click', e => port.postMessage({ target: 'popup', action: 'reload' }))
    })