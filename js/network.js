/**
 * Responds to network requests (`main_frame` & `xmlhttprequest`).
 * This is where data deshreding takes place.
 * 
 * @param {browser.webRequest} browser_request A `webRequest` instance
 * @param {array}              domains An array of domains to reduce network listening range and temper CPU [defaults: `<all_urls>`]
 * @param {function}           data_callback A function to be called when we have parsed valid data
 * @see https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/webRequest
 */
const network =
    (browser_request, domains, data_callback) => {
        if (data_callback && typeof data_callback == 'function') {
            data_callback_ = data_callback
        }

        const urls = domains == null ? ['<all_urls>'] : domains.map(item => `https://*${item}/*`)

        return {
            listen: value => network_listen_(browser_request, value, urls),
            listening: () => listening_,
            has_listener: () => network_has_listener_(browser_request)
        }
    }

/** A valuable piece of data which indicates that streaming data present a trait interest. */
const REGEX_ADVERTISERS_HOOK = /("payload":{"advertisers":)/
const REGEX_INTERESTS_HOOK = /("payload":{"interests":)/

/** Hooks over a small object that will provides us user id, name, gender, etc... */
const REGEX_USER_HOOK = /({"data":{"me":)(.*)(}}})/

/** Hooks over a shred of html that holds the client's language setting. */
const REGEX_LOCALE_HOOK = /<span lang="([a-z]{2}_[A-Z]{2})"/

/** A list of sub-states to indicate what type of URLs is watched. */
const LISTENING_IDLE = 0
const LISTENING_TRAITS = 1
const LISTENING_FACEBOOK = 2

/** Holds one of the above `int` constant for the actual listening state. */
let listening_ = LISTENING_IDLE

/** Keeps track of the streaming matrix object. */
let matrix_ = []

/** Keeps track of personal traits, which is a composition of advertisers' brands and interests. */
let traits_ = { attached: [], clicked: [], interests: [] }

/** Tracks the current trait's edge being streamed. */
let trait_edge_ = null

/** Keeps track of a facebook user. */
let user_ = null

/** Keeps track of facebook language preference. */
let locale_ = null

/** Data exchanges come in shreds, this holds value until it becomes `JSON` parseable. */
let data_stream_ = ''

/** Method to be called whenever we parse valid data. */
let data_callback_ = null

/**
 * `onBeforeRequest` callback
 * -> pipes relevant requests into bytes' streams...
 * 
 * @param {object} details Details about the request
 * @see https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/webRequest/onBeforeRequest#details
 */
const network_listener_ =
    details => {
        const filter = browser.webRequest.filterResponseData(details.requestId)

        filter.onstop = event => { filter.disconnect() }
        filter.onerror = event => { console.warn(`network filter error`) }

        filter.ondata =
            async event => {
                const decoder = new TextDecoder('utf-8')
                let plain_shred = decoder.decode(event.data)

                switch (listening_) {
                    case LISTENING_TRAITS:
                        if (plain_shred.match(REGEX_ADVERTISERS_HOOK)) { trait_edge_ = 'advertisers' }
                        else if (plain_shred.match(REGEX_INTERESTS_HOOK)) { trait_edge_ = 'interests' }

                        if (trait_edge_ != null) { merge_traits_(plain_shred) }
                        if (data_callback_) { data_callback_({ edge: 'traits', data: traits_ }) }

                        break
                    case LISTENING_FACEBOOK:
                        merge_user_(plain_shred)
                        network_listen_(browser.webRequest, LISTENING_IDLE)

                        break

                }

                filter.write(event.data)
            }

        return {}
    }
/**
 * Adds or removes webRequest listeners.
 * 
 * @param {boolean} value Whether to listen to the network or not
 * @param {array}   urls Array urls to filter requests listening
 */
const network_listen_ =
    (request, value, urls) => {
        listening_ = value
        const has_listener = network_has_listener_(request)

        if (listening_ != LISTENING_IDLE && !has_listener) {
            const filter = { urls: urls, types: ['xmlhttprequest', 'main_frame'] }
            request.onBeforeRequest.addListener(network_listener_, filter, ['blocking'])
        } else if (has_listener) {
            request.onBeforeRequest.removeListener(network_listener_)
        }
    }

/** Shortcuts to know of network is busy / listening. */
const network_has_listener_ = request => request.onBeforeRequest.hasListener(network_listener_)

const merge_user_ =
    incoming => {
        const user_match = incoming.match(REGEX_USER_HOOK)
        if (user_match) {
            try { user_ = JSON.parse(user_match[2]) }
            catch (e) { console.warn(e) }
        }

        const locale_match = incoming.match(REGEX_LOCALE_HOOK)
        if (locale_match) { locale_ = locale_match[1] }

        if (user_ && locale_) {
            if (data_callback_) { data_callback_({ edge: 'user', data: { user: user_, locale: locale_ } }) }
        }
    }
/**
 * When stream + input become JSON valid, merges an input array of traits with memory.
 * -> comparison is made on interest `fbid`.
 * 
 * @param {string} incoming Incoming shred of data
 * @returns {array} An array of traits or `null` if we can't decode.
 */
const merge_traits_ =
    incoming => {
        // strips input garbage: for (;;);
        if (data_stream_ == '') { incoming = incoming.substring(9, incoming.length) }

        let json_stream = null
        try { json_stream = JSON.parse(data_stream_ + incoming) }
        catch (e) {
            data_stream_ += incoming
            return null
        }

        if (trait_edge_ == 'advertisers') {
            const attached = json_stream.payload.advertisers.contact_info
            const clicked = json_stream.payload.advertisers.clicked

            attached.map(
                item => {
                    if (traits_.attached.findIndex(trait => item.fbid == trait.fbid) == -1) {
                        traits_.attached.push(item)
                    }
                })

            clicked.map(
                item => {
                    if (traits_.clicked.findIndex(trait => item.fbid == trait.fbid) == -1) {
                        traits_.clicked.push(item)
                    }
                })
        } else if (trait_edge_ == 'interests') {
            json_stream.payload.interests.map(
                item => {
                    if (traits_.interests.findIndex(trait => item.fbid == trait.fbid) == -1) {
                        traits_.interests.push(item)
                    }
                })
        }

        data_stream_ = ''
        trait_edge_ = null

        return traits_
    }