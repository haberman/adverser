const URL_TARGETS_API = 'http://ext.penumbra.me/adverser/api/'

const targets =
    () => {
        let js_zip_ = null
        let data_ = null
        let datastory_ = null

        return {
            download: async locale => {
                const dl = await targets_download_(locale)
                js_zip_ = dl.zip
                data_ = dl.data
                return true
            },
            list: () => targets_list_(js_zip_),
            country: () => country_,
            zip: () => js_zip_,
            data: () => data_,
            datastory: async traits => {
                if (js_zip_ == null) { return null }

                const list = await targets_list_(js_zip_)
                return targets_datastory_(data_, traits, list)
            }

        }
    }

const targets_list_ =
    async js_zip => {
        if (js_zip == null) { return null }

        const list = []
        for (let file of Object.keys(js_zip.files)) {
            const json_str = await js_zip.file(file).async('string')
            const json = JSON.parse(json_str)
            list.push({ file: file, content: json })
        }
        return list
    }

const targets_random_list =
    (search, amount) => { }

const targets_datastory_ =
    (data, traits, targets) => {
        const geo_i18n = [
            data.country.name,
            Math.round(data.country.count / data.count * 100),
            data.count]

        let story = browser.i18n.getMessage('story_geo', geo_i18n)

        const topics = []
        traits.interests.forEach(trait => {
            const topic_idx = topics.findIndex(topic => topic.topic == trait.topic)
            if (topic_idx != -1) {
                topics[topic_idx].traits.push(trait)
            } else {
                topics.push({ topic: trait.topic, traits: [trait] })
            }
        })

        topics.sort((a, b) => b.traits.length - a.traits.length)
        const topics_traits = topics[0].traits.concat(topics[1].traits)
        const topics_list = []
        for (let i = 0; i < 10; ++i) {
            const rand_idx = Math.ceil(Math.random() * topics_traits.length)
            topics_list.push(topics_traits[rand_idx].name)
        }

        const topics_i18n = [topics[0].topic, topics[1].topic, topics_list.reduce((acc, curr) => `${acc}, ${curr}`)]
        story += ' ' + browser.i18n.getMessage('story_topics', topics_i18n)

        // TODO
        // const companies_i18n = []
        
        return story
    }

/**
 * Returns the targets zip matching a given locale.
 * This will ask the unified `targets` API which defaults to `en_US` locale.
 * 
 * @param {string} locale The requested locale code
 */
const targets_download_ =
    async locale => {
        const targets = await xhr().request(URL_TARGETS_API, { data: { locale: locale } })
        const buffer = await xhr().request(targets.data.zip_file, { responseType: 'arraybuffer' })

        const js_zip = await JSZip.loadAsync(buffer)
        return { zip: js_zip, data: targets.data }
    }
